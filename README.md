
# Table of Contents

1.  [Prospectus](#org120dfe9)
2.  [Appraisal](#org45752ff)



<a id="org120dfe9"></a>

# Prospectus

(Unfinished) I need a way to communicate with myself, across devices, and control the code across untrusted networks. The server listener here responds to Telegram (first example, working now), and eventually hopefully Matrix/Riot and Signal. I want to be able to query my todos, calendar events, and run code on my home machines on demand.


<a id="org45752ff"></a>

# Appraisal

This is a lot of the way to being a minimal working product for myself. It&rsquo;s about the most overengineered chatbot ever, but I found it useful for figuring out Clojure async and parallel programming, SQL (hugsql, for persisting events in a log), and using LL Parsers (to parse the text input).

