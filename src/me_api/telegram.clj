(ns me-api.telegram
  (:require
   [clojure.edn :as edn]
   [clojure.java.io :as io]
   [me-api.state :as state]
   [morse.handlers :as h]
   [morse.polling :as poll]
   [morse.api :as api]
   [taoensso.timbre :refer [info error trace]]
   [me-api.db :as db]))

(defonce env (edn/read-string (slurp (io/file ".boot-env"))))
(defonce secret (get-in env [:main :secret]))

(defn respond! [{user-id :last-user
                 service-path :last-service-path
                 :as response}]
  (try
    (trace "response" response)
    (trace "telegram user-id" user-id)
    (api/send-text secret user-id (str response))
    (catch Exception e
      (let [data (ex-data e)]
        (error e)))))

(defn pure-dispatch [{{user-id :id} :chat
                      text :text
                      :as event}]
  (let [event (assoc event :service "telegram")]
    (swap! state/user-maps state/update-user event)))

(defonce respond-dispatch!
  (comp respond! pure-dispatch))

(h/defhandler bot-api
  (h/command-fn "start" (fn [{{id :id :as chat} :chat}]
                          (info "Bot joined new chat: " chat)
                          (api/send-text secret id "Welcome!")))

  (h/command "help" {{id :id :as chat} :chat}
             (api/send-text secret id "Help is on the way"))

  (h/command-fn "fetch" (fn [event] (info event)))

  (h/message-fn respond-dispatch!))

(defn telegram-channel []
  (let [channel-opts {:timeout 5}]      ;; 5 seconds
    (poll/start secret bot-api channel-opts)))
