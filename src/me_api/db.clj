(ns me-api.db
  (:require
   [me-api.db.event :as db-event]
   [clojure.core.async :refer [<! >! chan go go-loop sliding-buffer]]
   [clojure.string :as str]))

(defn run-sql [func & args]
  (try
    (apply func args)
    (catch org.sqlite.SQLiteException sql-exc
      (let [resultCode (.getResultCode sql-exc)]
        (throw
         (ex-info {:error-code (.-code resultCode)
                   :error-name (.name resultCode)
                   :error-message (.-message resultCode)}))))))

(defn dash-to-underscore-keys [event]
  (let [underscored (into {} (map
                              (fn [kee]
                                {kee
                                 (keyword (str/replace (name kee) "-" "_"))})
                              (keys event)))]
    (clojure.set/rename-keys event underscored)))

(defn enqueue-for-sql [ch event]
  (go (>! ch event)))

(defmulti handle-sql-event :sql-type)

(defmethod handle-sql-event :insert [event]
  (let [underscored (dash-to-underscore-keys event)]
    (run-sql db-event/insert-event! underscored)))

(defmethod handle-sql-event :select-one [{id :id}]
  ;; TODO This should probably be in response to a message
  (run-sql db-event/event-by-id id))

(defmethod handle-sql-event :default [params]
  (throw
   (ex-info {:error-name "handle-sql-event fallthrough"
             :error-message (str "handle-sql-event has no method for sql-type "
                                 (:sql-type params))
             :params params})))

(defn sql-consumer []
  (let [c (chan (sliding-buffer 64))]
    (go-loop []
      (let [data (<! c)]
        (when data
          (handle-sql-event data))
        (recur)))
    c))

(defonce sql-channel (sql-consumer))

(defn insert! [event])

(defn select [event])

(defn maps-to-tuples
  "Turn list of rows into list of tuples, sorted by keys."
  [rows]
  (let [sorted-keys (sort (keys (first rows)))]
    (map
     (fn [row]
       (map
        (fn [kee]
          (kee row))
        sorted-keys))
     rows)))
