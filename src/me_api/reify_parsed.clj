(ns me-api.reify-parsed
  (:require [com.rpl.specter :as sp]
            [taoensso.timbre :refer [info error trace]]))

;; Use for optionality?
(defn find-index-route [v data]
  (let [walker (sp/recursive-path [] p
                                  (sp/if-path sequential?
                                              [sp/INDEXED-VALS
                                               (sp/if-path [sp/LAST (sp/pred= v)]
                                                           sp/FIRST
                                                           [(sp/collect-one sp/FIRST) sp/LAST p])]))
        ret (sp/select-first walker data)]
    (if (or (vector? ret) (nil? ret)) ret [ret])))

;; (defprotocol ParsedType
;;   (user-merge [_ _]))

;; (defrecord TodoType
;;     [title items]
;;     ParsedType
;;     (user-merge [this user-state]
;;       ;; Define set assocation operations for user input
;;       ;; (If user attaches '-', that should never be added to the set,
;;       ;; whether it previously existed or not)
;;       (let [existing-todo (get-in user-state [:todo (:title this)] #{})
;;             operations
;;             (map
;;              (fn [row]
;;                (let [desc (:description row)]
;;                  (case (:toggle-flag row)
;;                    "+" [conj desc]
;;                    "-" [disj desc]
;;                    "" (if (contains? existing-todo desc)
;;                         [disj desc]
;;                         [conj desc]))))
;;              (:items this))]
;;         (assoc-in
;;          user-state
;;          [:todo (:title this)]
;;          (reduce (fn [acc [func desc]]
;;                    (func acc desc))
;;                  existing-todo
;;                  operations)))))

;; (defrecord CalendarType [date information])

(defmulti event-to-record
  "Take output of parser and put into constrained record format so all nested vector access happens here."
  (fn [_user-state event] (get-in event [:parsed 1 1])))

(defmethod event-to-record
  "todo"
  [user-state
   {:keys [user-id parsed service]}]
  (let [title (sp/select-one [2 1] parsed)
        service-path [service user-id :todo title]
        existing-todo (get-in user-state service-path #{})
        todo-list-items (into []
                              (map (fn [v]
                                     {:toggle-flag (get-in v [1 1])
                                      :description (get-in v [2 1])})
                                   (sp/select [3 sp/ALL vector?] parsed)))
        todo-item-operations (map
                              (fn [row]
                                (let [desc (:description row)]
                                  (case (:toggle-flag row)
                                    "+" [conj desc]
                                    "-" [disj desc]
                                    "" (if (contains? existing-todo desc)
                                         [disj desc]
                                         [conj desc]))))
                              todo-list-items)
        merged-todo-items (reduce (fn [acc [func desc]]
                                    (func acc desc))
                                  existing-todo
                                  todo-item-operations)
        final-user-state (assoc-in user-state (subvec service-path 2) merged-todo-items)]
    (trace "deep service path" service-path)
    (trace "final user state" final-user-state)

    [service-path final-user-state]))
