(ns me-api.core
  (:gen-class)
  (:require
   [me-api.telegram :as tlg]
   [me-api.state :as state]
   [me-api.db.event :as db-event]
   [taoensso.timbre :refer [info]]))

(defonce db-info {:classname "org.sqlite.JDBC"
                  :subprotocol "sqlite"
                  :subname "resources/events.db"})

(defn -main
  [& args]
  (db-event/create-event-table! db-info)
  (info "Starting bot listening")
  (tlg/telegram-channel)
  (let [all-user-state (state/get-user-maps)]
    (loop []
      (Thread/sleep 30000)
      (recur))))
