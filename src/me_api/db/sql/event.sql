-- :name create-event-table! :!
-- :doc Create event table
CREATE TABLE IF NOT EXISTS event (
       id INTEGER PRIMARY KEY AUTOINCREMENT,
       me_api_event_id VARCHAR,
       user_id INTEGER,
       service_timestamp TIMESTAMP,
       my_timestamp TIMESTAMP,
       text VARCHAR,
       event_type VARCHAR,
       event_name VARCHAR,
       event_json VARCHAR
)

-- :name drop-event-table! :!
-- :doc Drop event table if exists
DROP TABLE IF EXISTS event

-- :name insert-event! :! :n
-- :doc Insert a single event taken from the input stream
INSERT INTO event (me_api_event_id, user_id, service_timestamp, my_timestamp, text, event_type, event_name, event_json)
VALUES (:me_api_event_id, :user_id, :service_timestamp, :my_timestamp, :text, :event_type, :event_name, :event_json)

-- :name insert-events! :! :n
-- :doc Insert a single event taken from the input stream. Insertion order of keys is sorted alphabetically.
INSERT INTO event (me_api_event_id, event_json, event_name, event_type, service_timestamp, user_id)
VALUES :tuple*:events

-- A ":result" value of ":1" specifies a single record
-- (as a hashmap) will be returned
-- :name event-by-id :? :1
-- :doc Get event by id
SELECT * FROM event
WHERE id = :id

-- Let's specify some columns with the
-- identifier list parameter type :i* and
-- use a value list parameter type :v* for IN()
-- :name event-by-ids-specify-cols :? :*
-- :doc Event with returned columns specified
SELECT :i*:cols FROM event
WHERE id IN (:v*:ids)

-- :name event-by-type-and-name :? :*
-- :doc Standard select-- e.g. get all from "grocery" list in "/todo"
SELECT * FROM event
WHERE event_type = :event_type
AND event_name = :event_name

-- :name dumb-select-all :? :*
SELECT * FROM event
