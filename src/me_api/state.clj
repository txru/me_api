(ns me-api.state
  (:require
   [clojure.core.async :refer [chan go go-loop thread >!! >! <! close! alts!]]
   [clojure.edn :as edn]
   [clojure.spec.alpha :as s]
   [clojure.java.io :as io]
   [me-api.db :as db]
   [me-api.format :as fmt]
   [me-api.reify-parsed :as rp]
   [me-api.parse :as prs]
   [clojure.string :as str]
   [com.rpl.specter :as sp]
   [taoensso.timbre :refer [info error trace]])
  (:import
   java.util.UUID))

(defonce state-file (io/file "resources/persisted_state.edn"))

(defn load-state
  "Invoked on startup. Load state for all users from flat file."
  []
  (edn/read-string state-file))

(defn dump-state
  "Invoked on shutdown. Save state to flat file."
  [user-state]
  (spit state-file (pp/pprint user-state)))

(defn get-user-maps []
  (let [loaded (load-state)]
    (if (empty? loaded)
      (atom {})
      loaded)))

(defn create-request-id [event]
  (let [uuid (str (UUID/randomUUID))]
    (assoc event :me_api_event_id uuid)))

(defn telegram-transform-event
  "Add event metadata for SQL etc."
  [{{user-id :id}     :chat
    text              :text
    service-timestamp :date
    service           :service
    :as               event}]
  (trace "chat-user-id" user-id)
  (trace "chat" (:chat event))
  (let [my-timestamp (quot (System/currentTimeMillis) 1000)]
    (merge event
           {:user-id           user-id
            :text              text
            :service-timestamp service-timestamp
            :my-timestamp      my-timestamp})))

(defn update-user
  [all-users {{user-id :id} :from
              service :service
              :as event}]
  (trace "event" event)
  (trace "fetched user-id" user-id)
  (trace "service" service)
  (let [service-path [service user-id]
        user-state (get all-users service-path {})
        handled-event (try
                        (-> event
                            create-request-id
                            prs/parse-message
                            telegram-transform-event)
                        (catch Exception e
                          (let [data (ex-data e)]
                            (error e)
                            (assoc user-state :error data))))
        [deep-path updated-user-state] (rp/event-to-record user-state handled-event)]
    (trace "updated user state" updated-user-state)
    (-> user-state
        (assoc :last-user user-id)
        (assoc :last-service-path deep-path)
        (assoc-in service-path updated-user-state))))

(defn retrieve [user event-type event-name])

(defn create-producer [running]
  (let [updates (chan)]
    (go-loop []
      (let [response]))))

(defn create-consumer)

(defn start []
  (let [running (chan)]))
