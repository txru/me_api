(ns me-api.parse
  (:require
   [instaparse.core :as insta]
   [taoensso.timbre :refer [info]]))

(defonce parser (insta/parser (slurp "resources/insta.txt")))

(defn parse-message [{event-id :me_api_event_id
                      message :text
                      :as event}]
  (let [response (parser message)
        event-type (keyword (get-in response [1 1]))
        validate-parse (fn [resp] (nil? (:index resp)))
        returned (merge
                  event
                  {:parsed response
                   :event-type event-type})]
    (info (str event-id " (parse-message): " "<<<" message ">>>"))
    (if (validate-parse response)
      returned
      (throw
       (ex-info "Failed to parse successfully"
                {:error-name "parse-message"
                 :error-message response})))))
