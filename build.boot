(def project 'me_api)
(def version "0.1.0-SNAPSHOT")

(set-env! :resource-paths #{"resources" "src"}
          :source-paths   #{"test"}
          :dependencies   '[[org.clojure/clojure "RELEASE"]
                            [adzerk/boot-test "RELEASE" :scope "test"]
                            [boot-cljfmt "0.1.3" :scope "test"]

                            ;; SQL
                            [com.layerware/hugsql "0.5.1"]
                            [org.xerial/sqlite-jdbc "3.32.3.2"]
                            ;; JSON
                            [cheshire "5.10.0"]
                            ;; Logging
                            [com.taoensso/timbre "5.1.2"]
                            ;; Parsing
                            [instaparse "1.4.10"]
                            ;; Railway-oriented-programming
                            ;; This was a mistake
                            ;; [rop "0.4.1"]
                            ;; Structure access
                            [com.rpl/specter "1.1.3"]
                            ;; [clojure.java-time "0.3.2"]
                            ;; Telegram API
                            [morse "0.4.3"]])

(task-options!
 aot {:namespace   #{'me-api.core}}
 pom {:project     project
      :version     version
      :description "FIXME: write description"
      :url         "http://example/FIXME"
      :scm         {:url "https://github.com/yourname/me_api"}
      :license     {"Eclipse Public License"
                    "http://www.eclipse.org/legal/epl-v10.html"}}
 repl {:init-ns    'me-api.core}
 jar {:main        'me-api.core
      :file        (str "me_api-" version "-standalone.jar")})

(deftask build
  "Build the project locally as a JAR."
  [d dir PATH #{str} "the set of directories to write to (target)."]
  (let [dir (if (seq dir) dir #{"target"})]
    (comp (aot) (pom) (uber) (jar) (target :dir dir))))

(deftask run
  "Run the project."
  [a args ARG [str] "the arguments for the application."]
  (with-pass-thru fs
    (require '[me-api.core :as app])
    (apply (resolve 'app/-main) args)))

(require '[adzerk.boot-test :refer [test]]
         '[boot-cljfmt.core :refer [check fix]])
