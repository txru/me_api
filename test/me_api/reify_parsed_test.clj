(ns me-api.reify-parsed-test
  (:require [me-api.reify-parsed :as sut]

            [com.rpl.specter :as sp]

            [clojure.test :refer [deftest testing is]]))

(defonce parsed-message-event
  {:parsed
   [:message
    [:event-type "todo"]
    [:event-name "shopping list"]
    [:event
     [:todo [:toggle-flag "+"] [:item-description "dog food"]]
     [:todo [:toggle-flag "-"] [:item-description "cat food"]]
     [:todo [:toggle-flag "-"] [:item-description "hamster food"]]
     [:todo [:toggle-flag ""] [:item-description "bear food"]]
     [:todo [:toggle-flag ""] [:item-description "people food"]]]]})

(deftest todo-record
  (testing "simple intake"
    (let [expected #{"bear food" "people food" "dog food"}
          response (sut/event-to-record {:empty-user-state []} parsed-message-event)
          actual (get-in response [:todo "shopping list"])]
      (is (= expected actual))))

  (testing "merges"
    (testing "simple expectations"
      (let [expected #{"parrot food" "dog food" "bear food"}
            user-state {:todo {"shopping list" #{"people food" "cat food" "parrot food"}}}
            actual-state (sut/event-to-record user-state parsed-message-event)]
        (is (= expected (get-in actual-state [:todo "shopping list"])))))

    (testing "preserves other data"
      (let [expected {:todo {"shopping list" #{"dog food" "bear food" "people food"}}
                      :calendar "arbitrary data"
                      :partial-service-path ["telegram" 12341234]
                      :service-path ["telegram" 12341234 :todo "shopping list"]}
            user-state {:calendar "arbitrary data"
                        :partial-service-path ["telegram" 12341234]
                        :service-path ["telegram" 12341234 :todo "shopping list"]}
            actual (sut/event-to-record user-state parsed-message-event)]
        (is (= expected actual))))

    (testing "empty initial set"
      (let [expected #{"dog food" "bear food" "people food"}
            user-state {:todo {"shopping list" #{}}}
            actual-response (sut/event-to-record user-state parsed-message-event)
            actual (get-in actual-response [:todo "shopping list"])]
        (is (= expected actual))))))
