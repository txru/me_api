(ns me-api.parse-test
  (:require
   [me-api.parse :as sut]
   [clojure.test :refer :all]))

(deftest test-parse-message
  (testing "Parse todo"

    (testing "simple case"
      (let [text "todo title-here\n+dog food\n-cat food\npeople food"
            expected
            {:text text
             :event-type :todo
             :parsed
             [:message
              [:event-type "todo"]
              [:event-name "title-here"]
              [:event
               [:todo [:toggle-flag "+"] [:item-description "dog food"]]
               [:todo [:toggle-flag "-"] [:item-description "cat food"]]
               [:todo [:toggle-flag ""] [:item-description "people food"]]]]}
            actual (sut/parse-message
                    {:text text})]
        (is (= expected actual))))

    (testing "containing symbols"
      (let [text "todo title!-here@#$\n+dog food#$#$"
            expected
            {:text text
             :event-type :todo
             :parsed
             [:message
              [:event-type "todo"]
              [:event-name "title!-here@#$"]
              [:event [:todo [:toggle-flag "+"] [:item-description "dog food#$#$"]]]]}
            actual (sut/parse-message
                    {:text text})]
        (is (= expected actual))))

    (testing "Exceptions"
      (testing "Empty"
        (let [text "todo title here"]
          (try
            (sut/parse-message {:text text})
            (catch Exception e
              (is (= "parse-message"
                     (:error-name (ex-data e))))))
          (is (thrown? Exception (sut/parse-message {:text text}))))))))
