(ns me-api.db-test
  (:require
   [me-api.db :as sut]
   [me-api.telegram-test :as tt]
   [clojure.test :refer :all]))

(deftest test-map-to-tuple
  (testing "basic map to tuple"
    (let [data [{:e 10 :d 9 :c 8 :b 7 :a 6}
                {:e 20 :d 19 :c 18 :b 17 :a 16}]
          expected [[6 7 8 9 10] [16 17 18 19 20]]
          actual (sut/maps-to-tuples data)]
      (is (= expected actual)))))

(deftest test-dash-to-underscore
  (testing "function specs"
    (let [m {:a-b-c 1 :b-c-d 2}
          expected {:a_b_c 1 :b_c_d 2}
          actual (sut/dash-to-underscore-keys m)]
      (is (= expected actual)))))
