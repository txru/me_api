(ns me-api.telegram-test
  (:require [me-api.telegram :as sut]
            [clojure.test :refer :all]
            [me-api.state :as state]))

(def base-telegram-message
  {:message_id 151
   :from {:id 12341234
          :is_bot false
          :first_name "Kevin"
          :username "not_lyterk"
          :language_code "en"}
   :chat {:id 12341234
          :first_name "Kevin"
          :username "not_lyterk"
          :type "private"}
   :date 1604106162
   :service "telegram"
   :partial-service-path ["telegram" 12341234]
   :text "todo shopping list\ncat food"
   :entities [{:offset 0
               :length 5
               :type "bot_command"}]})

(deftest pure-dispatch-test
  (testing "base message"
    (reset! state/user-maps {})
    (is (= [["telegram" 12341234] {"telegram" {12341234 {:error nil}}}]
           (sut/pure-dispatch base-telegram-message)))))
