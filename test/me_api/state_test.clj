(ns me-api.state-test
  (:require
   [me-api.state :as sut]
   [me-api.db :as db]
   [me-api.telegram-test :as tt]
   [taoensso.timbre :refer [info error]]
   [clojure.core.async :refer [<!! chan go]]
   [clojure.test :refer :all]))

(defonce user-id 12341234)

(defonce test-event-todo
  {:message_id 259
   :from
   {:id 12341234 :is_bot false :first_name "Kevin" :username "not_lyterk" :language_code "en"}
   :chat
   {:id 12341234 :first_name "Kevin" :username "not_lyterk" :type "private"}
   :date 1625233454 :text "todo hello\nsomething" :service "telegram"})
;; aeou

;; (deftest test-get-user-state!
;;   (testing "insert new user"
;;     (sut/get-user-state! 1234)
;;     (let [expected {}
;;           actual @(:state (get @sut/user-maps 1234))]
;;       (is (= expected actual)))
;;     (reset! sut/user-maps {}))
;;   (testing "get existing user"
;;     (sut/get-user-state! 1234)
;;     (let [expected {}
;;           actual (:state (get @sut/user-maps 1234))]
;;       (is (= expected actual)))
;;     (let [expected {}
;;           actual (:state (sut/get-user-state! 1234))]
;;       (is (= expected actual)))
;;     (reset! sut/user-maps {})))


(deftest test-todo-stuff
  "Save event from beginning to end. Pass an empty queue to save-event, then pop
  that queue-- we should have the event we entered, plus the necessary changes.
  This does not need SQLite or anything."
  (let [test-message (merge
                      tt/base-telegram-message
                      {:text "todo shopping list\nadd something"
                       :event_type :todo})
        service-path ["telegram" user-id :todo "shopping list"]
        all-users-state (atom {})]
    (testing "update-user :todo"
      (let [expected #{"add something"}
            event-response (swap! all-users-state sut/update-user test-message)
            actual (get-in event-response service-path)]
        (is
         (= expected actual))))
    (testing "not with atom"
      (let [expected #{"add something"}
            event-response (sut/update-user {} test-message)
            actual (get-in event-response service-path)]
        (is
         (= expected actual))))
    (testing "with atom multiple update-user calls"
      (reset! all-users-state {})
      (let [event-response (swap! all-users-state sut/update-user test-message)
            expected #{"add something"}
            actual (get-in event-response service-path)]
        (is
         (= expected actual)))))
  (testing "end-to-end"
    (let [all-users-state (atom {})
          expected #{"something"}
          event-response (swap! all-users-state sut/update-user test-event-todo)
          actual (get-in event-response ["telegram" user-id :todo "hello"])]
      (is (= expected actual)))))
